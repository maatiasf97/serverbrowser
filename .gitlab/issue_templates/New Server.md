Name (Maximum length 35 characters):
Description: 
Hostname or IP address:
Port (if not the default of 14004):
Location (ISO-3166-1 Alpha-2 code - see https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)

- [ ] I confirm that the server is configured to auto-update to the Weekly release channel
- [ ] I confirm that the server is using the official auth server - https://auth.veloren.net